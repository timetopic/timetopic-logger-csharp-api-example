﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Using WebsocketSharp for sockets
using WebSocketSharp;

namespace timetopic_logger_csharp_api_example
{

    /// <summary>  
    ///  Exception when SocketLogger initialization fails
    /// </summary>  
    public class SocketLoggerInitFailureException : Exception
    {
        // Initializing Socket failed
    }

    /// <summary>  
    ///  Exception when something fatal happens on transmitter
    /// </summary>  
    public class SocketLoggerFailureException : Exception
    {
        // Operating Socket failed
    }

    /// <summary>  
    ///  This class will take care of transmitting loglines to TimeToPic Logger
    /// </summary>  
    class SocketLogger
    {
        private static SocketLogger Instance = null;

        private WebSocket mWebsocket =null;

        // Localhost access
        private string mAddress = "ws://127.0.0.1:5001";

        SocketLogger()
        {
            try { 
                mWebsocket = new WebSocket(mAddress);
                mWebsocket.Connect();
                mWebsocket.OnError += MWebsocket_OnError;
            }
            catch // whatever comes in
            {
                throw new SocketLoggerInitFailureException();
            }            
        }

        private void MWebsocket_OnError(object sender, ErrorEventArgs e)
        {
            mWebsocket.Close();
        }

        public static void SendSocket(string data)
        {
            if (Instance==null)
            {
                try { 
                    Instance = new SocketLogger();
                }
                catch (SocketLoggerInitFailureException)
                {
                    throw new SocketLoggerFailureException();
                }
            }
            Instance.SendWebSocket(data);
        }
       
        private void SendWebSocket(string data)
        {
            if ( mWebsocket.IsAlive==true)
            {
                try
                {
                    mWebsocket.Send(data);
                }
                catch (System.Threading.ThreadAbortException)
                {
                    // thread is aborted. Just exit.
                }                
            }           
            else
            {
                Console.WriteLine("No Socket. Writing data here:" +data);
            }
        }
    }
}
