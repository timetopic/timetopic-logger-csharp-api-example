﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace timetopic_logger_csharp_api_example
{
    class Program
    {

        /* Application start */
        static void Main(string[] args)
        {
            /* Create worker threads that sends logging data. Lets check some parallelism */
            const int threadCountForThisSample = 20;
            List<Thread> threadList = new List<Thread>();
            for (int k = 0; k < threadCountForThisSample; k++)
            {
                /* Create thread and give entry point */
                Thread t = new Thread(MyWorkerThread);          
                t.Start(); // start job.
                threadList.Add(t); // store handle 
            }
            Console.Write("Running application and sending log data to websocket. Press enter to stop." + Environment.NewLine);
            Console.ReadLine(); 

            // In case threads still active, stop them 
            foreach (Thread t in threadList)
            {
                if ( t.IsAlive==true) { 
                    t.Abort();
                    t.Join();
                }
            }
            Console.Write("Example completed. Press enter to stop" + Environment.NewLine);
            Console.ReadLine();
        }

        // Worker thread that executes our sample code. 
        static void MyWorkerThread()
        {
            const int defaultIterationsForExample = 1000;
            const int defaultJobMaxWaitTimeMs = 10;
            const int defaultMaxSleepTimeMsAfterJob = 50;

            var threadId = Thread.CurrentThread.ManagedThreadId;
            var myEvent = "myEvent in thread #" + threadId.ToString();

            try { 
                for (int k = 0; k < defaultIterationsForExample; k++)
                {
                    // payload work. Event that has startAnd stop 
                    performEventExample(defaultJobMaxWaitTimeMs, defaultMaxSleepTimeMsAfterJob, myEvent);
                }
                string msg = "Thread #" + threadId.ToString() + " completed" + Environment.NewLine;
                Console.Write(msg);
            }
            catch (ThreadAbortException)
            {
                string msg = "Thread #" + threadId.ToString() + " aborted" + Environment.NewLine;
                Console.Write(msg);
            }
        }

        private static void performEventExample(int defaultJobMaxWaitTimeMs, int defaultMaxSleepTimeMsAfterJob, string myEvent)
        {
            TimeToPicLogger.LogEventStart(myEvent);     // start event. This is used for measurement
            Random rnd = new Random();
            int jobTimeTimeMs = rnd.Next(1, defaultJobMaxWaitTimeMs);
            Thread.Sleep(jobTimeTimeMs);            // do simulated work 
            TimeToPicLogger.LogEventStop(myEvent);      // stop event. This stops measurement

            Random rnd2 = new Random();
            int sleepTimeMs = rnd2.Next(1, defaultMaxSleepTimeMsAfterJob);
            Thread.Sleep(sleepTimeMs);
        }
    }
}
