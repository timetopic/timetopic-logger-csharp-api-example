﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace timetopic_logger_csharp_api_example
{
    /// <summary>  
    ///  Class for sending TimeToPic compatible logging messages
    /// </summary>  
    class TimeToPicLogger
    {
        private static DateTime sStartTime = DateTime.Now;
        private static NumberFormatInfo sNfi = new CultureInfo("en-US", false).NumberFormat;

        public static void LogEventStart(string eventName)
        {
            string logLine = getTimeStamp().ToString() + ";event;start;" + eventName + Environment.NewLine;            
            send(logLine);
        }

        public static void LogEventStop(string eventName)
        {
            string logLine = getTimeStamp()+ ";event;stop;" + eventName + Environment.NewLine;
            send(logLine);
        }

        public static void LogState(string newState, string machineName)
        {

        }

        public static void LogValue(int valueToBeLogged, string nameOfVálue)
        {

        }

        private static string getTimeStamp()
        {
            // https://timetopic.herwoodtechnologies.com/documentation/file-format
            double milliseconds = (DateTime.Now - sStartTime).TotalMilliseconds;
            double secs = milliseconds / 1000.0;            
            sNfi.NumberDecimalSeparator = ".";
            string ret = secs.ToString(sNfi);
            return ret;
        }

        private static void send(string logLine)
        {
            SocketLogger.SendSocket(logLine);
        }

    }
}
